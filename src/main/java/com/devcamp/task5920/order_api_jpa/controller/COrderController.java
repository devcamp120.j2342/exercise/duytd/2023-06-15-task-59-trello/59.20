package com.devcamp.task5920.order_api_jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import com.devcamp.task5920.order_api_jpa.model.COrder;
import com.devcamp.task5920.order_api_jpa.repository.COrderRepository;
@CrossOrigin
@RestController
public class COrderController {
    @Autowired
    COrderRepository pOrderRepository;

    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrder() {

        try{
            List<COrder> allOrder = new ArrayList<>();

            pOrderRepository.findAll().forEach(allOrder::add);
            return new ResponseEntity<List<COrder>>(allOrder, HttpStatus.OK);
        }catch(Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
