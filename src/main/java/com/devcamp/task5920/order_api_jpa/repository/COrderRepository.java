package com.devcamp.task5920.order_api_jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5920.order_api_jpa.model.COrder;

public interface COrderRepository extends JpaRepository<COrder, Long> {
    
}
