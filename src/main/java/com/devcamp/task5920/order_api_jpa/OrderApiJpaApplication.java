package com.devcamp.task5920.order_api_jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderApiJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderApiJpaApplication.class, args);
	}

}
